package com.sean.codarchive;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;

@SpringBootApplication
public class CodarchiveApplication {

	private static final String PROPERTIES =
			"spring.config.location="
					+"classpath:/application.yml"
					+",classpath:/credentials.yml";

	public static void main(String[] args) {
		new SpringApplicationBuilder(CodarchiveApplication.class)
				.properties(PROPERTIES)
				.run(args);
		//SpringApplication.run(CodarchiveApplication.class, args);
	}

}
