package com.sean.codarchive.api.controller;

import com.sean.codarchive.api.mapper.TestMapper;
import com.sean.codarchive.api.model.TestUser;
import com.sean.codarchive.api.service.TestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/test")
public class TestController {

    @Autowired
    TestService testService;

    @RequestMapping(value = "/all", method = RequestMethod.GET)
    public ResponseEntity<List<TestUser>> getall(){
        try {
            return new ResponseEntity(testService.getAll(), HttpStatus.OK);
        } catch(Exception e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<TestUser> insert(@RequestBody TestUser testUser){
        try {
            testService.insertUser(testUser);
            return new ResponseEntity<>(HttpStatus.CREATED);
        } catch(Exception e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<List<TestUser>> getUserById(@PathVariable("id") int id) throws Exception{
        try {
            return new ResponseEntity(testService.getUserById(id), HttpStatus.OK);
        } catch(Exception e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }



}
