package com.sean.codarchive.api.service;

import com.sean.codarchive.api.mapper.TestMapper;
import com.sean.codarchive.api.model.TestUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TestService {

    @Autowired
    TestMapper testMapper;

    public List<TestUser> getAll() throws Exception {
        return testMapper.selectAll();
    }

    public TestUser getUserById(int id) throws Exception {
        TestUser user = testMapper.selectById(id);
        System.out.println(user);
        return user;
    }

    public void insertUser(TestUser testUser) throws Exception {
        testMapper.insertUser(testUser);
    }

}
