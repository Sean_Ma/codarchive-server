package com.sean.codarchive.api.model;

import lombok.*;

@Getter
@Setter
@EqualsAndHashCode
@ToString
public class TestUser {
    private @NonNull int _id;
    private @NonNull String name;
    private String mobile;

    @Builder
    public TestUser(String name, String mobile) {
        this._id = -1;
        this.name = name;
        this.mobile = mobile;
    }
}
