package com.sean.codarchive.api.model;

import lombok.*;

import java.sql.Timestamp;

enum Status {
    R, D
}

enum IsAdmin {
    N, Y
}

@Getter
@Setter
@ToString
@EqualsAndHashCode(of={"id", "email"})
public class User {
    @NonNull private int _id;
    @NonNull private String userid;
    @NonNull private String email;
    @NonNull private String password;
    @NonNull private String username;
    @NonNull private Timestamp regtime;
    @NonNull private Status status;
    @NonNull private IsAdmin isadmin;

    @Builder
    public User(String userid, String email, String password, String username) {
        this._id = -1;
        this.userid = userid;
        this.email = email;
        this.password = password;
        this.username = username;
    }
}
