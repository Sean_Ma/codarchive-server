package com.sean.codarchive.api.mapper;

import com.sean.codarchive.api.model.TestUser;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface TestMapper {
    TestUser selectById(int id);
    List<TestUser> selectAll();
    void insertUser (TestUser user);
}
