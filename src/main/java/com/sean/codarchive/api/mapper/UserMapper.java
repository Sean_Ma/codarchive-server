package com.sean.codarchive.api.mapper;

import com.sean.codarchive.api.model.TestUser;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface UserMapper {
    void addUser (TestUser user);

}
